/**
 * \file main.cc
 * \brief Main file
 * \author petit_w
 *
 */

#include "csvParser.hh"
#include <iostream>
#include <experimental/filesystem>
#include <regex>

#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

namespace fs = std::experimental::filesystem;

/**
 * \fn void bdd_manager(CsvParser& parser, sql::Connection *con)
 * \brief Parse the file and send each entry to the MySQL database
 *
 * \param parser CsvParser reference already initialize
 * \param con Connection pointer to the already connect to the database
 */
void bdd_manager(CsvParser& parser, sql::Connection *con)
{
  sql::Statement *stmt = con->createStatement();
  Entry e;
  int result = 0;
  for (int line_number = 0; (result = parser.next(&e)) != 2; ++line_number)
    {
      if (result == 1)
	{
	  std::cerr << "Error: File " + parser.path_get() + " line " <<
	    line_number << ": Bad input" << std::endl;
	  continue;
	}
      std::cout << e.first_name << std::endl;
      try {
	stmt->execute("INSERT INTO client VALUES ('" + e.first_name + "', '"+ e.last_name
		      + "', '" + e.email + "', '" + e.date + "');");
      } catch (sql::SQLException &e) {
	if (e.getErrorCode() == 1062)
	  {
	    std::cerr << "Client already in the database" << std::endl;
	  }
	else
	  {
	    std::cout << "# ERR: SQLException in " << __FILE__;
	    std::cout << "(" << __FUNCTION__ << ") on line " 
		      << __LINE__ << std::endl;
	    std::cout << "# ERR: " << e.what();
	    std::cout << " (MySQL error code: " << e.getErrorCode();
	    std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	  }
      }
    }
}

/**
 * \fn void watch_directory(sql::Connection *con, std::string path, std::string regex_string)
 * \brief Watch over a directory files to send to the database
 *
 * \param con Connection pointer to the already connect to the database
 * \param path a string to the directory to watch
 * \param regex_string a valid regular expression string corresponding to the file to look for
 * This function find recursively all files that match the regex and try to open them.
 * If the CsvParser is create successfuly for this file, call the bdd_manager with this parser.
 */
void watch_directory(sql::Connection *con, std::string path, std::string regex_string)
{
  std::regex regex(regex_string);
  for (auto& p: fs::recursive_directory_iterator(path))
    {
      std::string file_path = p.path().string();
      int last_separator = file_path.rfind("/") + 1;
      std::string file_name = file_path.substr(last_separator, file_path.size() - last_separator);
      if (!std::regex_match(file_name, regex))
	continue;
      CsvParser parser(file_path);
      if (!parser.init())
	{
	  std::cerr << "Error: Cannot open " + file_path + " file" << std::endl;
	  std::cerr << "Skiping this file" << std::endl;
	}
      bdd_manager(parser, con);
    }
}

/**
 * \fn int main(int argc, char* argv[])
 * \brief Main function of the program
 *
 * \param argc number of arguments
 * \param argv array containing arguments
 * The function check arguments and try to open a connection with the MySQL server. If the
 * connection is successful, a "client" table is create, and the watch_directory function
 * call with the directory give in parameter.
 */
int main(int argc, char* argv[])
{
  if (argc < 5)
    {
      std::cerr << "Missing arguments\nUsage: app DIRECTORY NAMEROOT PWDROOT BDDNAME [REGEX]"
		<< std::endl;
      exit(1);
    }

  std::string regex = ".*";
  if (argc > 5)
    regex = argv[5];
  sql::Driver *driver;
  sql::Connection *con;
  sql::Statement *stmt;
  driver = get_driver_instance();
  try {
    con = driver->connect("tcp://127.0.0.1:3306", argv[2], argv[3]);
    con->setSchema(argv[4]);
  } catch (sql::SQLException &e) {
    std::cerr << "Error: Cannot connect to the database" << std::endl;
    exit(2);
  }

  stmt = con->createStatement();
  try {
    std::cout << "Creation client table" << std::endl;
    stmt->execute("CREATE TABLE client (first_name VARCHAR(50), last_name VARCHAR(50), email VARCHAR(100), date DATETIME, PRIMARY KEY (last_name, first_name, email, date));");
  } catch (sql::SQLException &e) {
    std::cout << "Client table already found. Adding new clients" << std::endl;
  }
  delete stmt;

  watch_directory(con, argv[1], regex);
  std::cout << "Done" << std::endl;
  delete con;
  return 0;
}
