#include <string>

/**
 * \struct Entry
 * \brief C-Like struct representing a record
 *
 */
struct Entry
{
  std::string first_name; /*!< First name of the client */
  std::string last_name; /*!< Last  name of the client */
  std::string email; /*!< Eail of the client */
  std::string date; /*!< Birth date of the client */
};
