/**
 * \file csvParser.cc
 * \brief Implementation of the Parser class
 * \author petit_w
 *
 */

#include "csvParser.hh"
#include <sstream>
#include <iostream>
#include <regex>

bool CsvParser::init()
{
  stream_.open(path_);
  return stream_.is_open();
}

int CsvParser::next(Entry *e)
{
  std::string line;
  std::getline(stream_, line);
  if (line.empty())
    return 2;
  std::stringstream stream_line(line);
  std::string token;
  std::getline(stream_line, token, ',');
  if (token.size() > 50)
    return 1;
  e->first_name = token;
  std::getline(stream_line, token, ',');
  if (token.size() > 50)
    return 1;
  e->last_name = token;
  std::getline(stream_line, token, ',');
  std::regex email_regex("(?:(?:[^<>()\\[\\].,;:\\s@\"]+(?:\\.[^<>()\\[\\].,;:\\s@\"]+)*)|\".+\")@(?:(?:[^<>()‌​\\[\\].,;:\\s@\"]+\\.)+[^<>()\\[\\].,;:\\s@\"]{2,})");
  if (token.size() > 100 || !std::regex_match(token, email_regex))
    return 1;
  e->email = token;
  std::getline(stream_line, token, ',');;
  if (token.size() != 20)
    return 1;
  token.replace(10, 1, " ");
  e->date = token.substr(0, token.size() - 1);
  if (!stream_line.eof())
    return 1;
  return 0;
}
