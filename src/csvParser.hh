#pragma once

/**
 * \file csvParser.hh
 * \brief Header of the Parser class
 * \author petit_w
 *
 */

#include <string>
#include <fstream>
#include "entry.hh"

/**
 * \class CsvParser
 * \brief Parser class for csv
 *
 * This class is a very basic csv parser, with a Next function that give the next following entry.
 */
class CsvParser
{
public:
  /**
   *  \brief Constructor
   *  Constructor of the CsvParser class
   *  \param path is the path of the file to parse
   */
  CsvParser(std::string path)
    : path_(path)
  {
  }

  /**
   * \brief Initialization function
   * Initialize the parser and try to open the file
   * \return True if the file is open and ready to be read, false otherwise
   */
  bool init();

  /**
   * \brief Tokenizer
   * Parse the next line of the file. Fill the entry given in parameter with token
   * param e the entry structure to fill with token of the current line. Check the validity of
   * each record (no more than 4 field, size of field) and the validity of the email.
   * \return 0 if entry is correctly fill, 1 if an error is detected on the line, 2
   * if the EOF is reached
   */
  int next(Entry *e);

  std::string path_get()
  {
    return path_;
  }

private:
  /**
   * Path of the file to parse
   */
  std::string path_;
  /**
   * Stream of the file corresponding to the path_
   */
  std::ifstream stream_;
};
