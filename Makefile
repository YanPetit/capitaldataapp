CXX=g++
CXXFLAGS= -I/usr/include/cppconn -std=c++17 -g -pedantic -Wall -Wextra -Werror
OBJ=src/main.o src/csvParser.o
all: $(OBJ)
	g++ $(OBJ) -o app -lstdc++fs -lmysqlcppconn

clean:
	rm -f app *.o src/*.o

docs:
	@doxygen doxygen/Doxyfile
	firefox doxygen/html/index.html
